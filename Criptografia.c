#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define TAM 8
#define SYMBOLS 256
#define TEMPF "arquivoTemporario.txt"

typedef unsigned char byte;

//Esta funcao exibe uma mensagem de erro e encerra
//a execucao do programa caso haja algum problema
void encerrar(char *s){
	perror(s);
    	exit(1);
}

/*
Funcao hash para resumir uma string
Retorna um valor com 4 casas decimais sempre
para strings de ate 8 bytes de comprimento, que e' o padrao
do codigo
*/
unsigned int hash(unsigned char *str){
        unsigned int hash = 0;
        int c;

        while ((c = *str++)){
        	hash = (hash * 0.79) + c;
        }
        hash += 1000;

        return hash;
}

void geraChavePublica(byte *chavePub, byte *chavePriv){
	int i;
	for(i = 0; i < TAM; i++){
		chavePub[i] = SYMBOLS - chavePriv[i];
	}
}

void criptografiaAssimetrica(byte *bloco, byte *chave, int itens)
{
	int i;
	char temp;
  	for(i = 0; i < itens; i++) 
  	{
  		//Criptografando a mensagem com base na chave privada do emissor
    		bloco[i] = (bloco[i] + chave[i]) % SYMBOLS;

	}
	//Invertendo a ordem dos caracteres
	for(i = 0; i < (itens / 2); i++){
		temp = bloco[itens - (i + 1)];
		bloco[itens - (i + 1)] = bloco[i];
		bloco[i] = temp;
	}	
}


void criptografiaSimetrica(byte *bloco, byte *chave, int itens)
{
	//Para o receptor conseguir decriptografar a mensagem
	//criptografada com este codigo, ele devera' saber
	//o valor da chave utilizada para a criptografia
	int i;
	char temp;
  	for(i = 0; i < itens; i++) 
  	{
  		if(i % 2 == 0){
  			bloco[i] = bloco[i] + (chave[i] >> 1);
  		}else{
  			bloco[i] = bloco[i] + (chave[i] << 1);
  		}
	}
	//Invertendo a ordem dos caracteres
	for(i = 0; i < (itens / 2); i++){
		temp = bloco[itens - (i + 1)];
		bloco[itens - (i + 1)] = bloco[i];
		bloco[i] = temp;
	}	
}

int main()
{
	byte BlocoDados[TAM + 1], chavePriv[TAM], chavePub[TAM], chaveSim[TAM];
	long cont = 0;
	char tamMsg[32], hashString[5];
  	FILE *ArquivoEntrada, *ArquivoSaida, *tempF;
  	char NomeArquivoEntrada[30], NomeArquivoSaida[30], concatHash[TAM + 1];
  	unsigned int Itens, len, hashResult;

  	printf("Entre com o Nome do Arquivo Original = ");
  	scanf("%s", NomeArquivoEntrada);
  	printf("Entre com o Nome do Arquivo para Cifragem = ");
  	scanf("%s", NomeArquivoSaida);
  	printf("Entre com a chave privada de 8 digitos = ");
  	scanf("%s", chavePriv);
  	printf("Entre com a chave simetrica de 8 digitos = ");
  	scanf("%s", chaveSim);
  
 	if((ArquivoEntrada  = fopen(NomeArquivoEntrada, "rb")) == NULL){
 		encerrar(NomeArquivoEntrada);
 	}
  	if((ArquivoSaida = fopen(NomeArquivoSaida, "wb")) == NULL){
  		encerrar(NomeArquivoSaida);
  	}
  	//Arquivo de auxilio para concatenar a mensagem à string gerada a partir do resumo hash + criptografia
 	//Este arquivo esta' sendo utilizado para nao precisar salvar uma string muito grande (se houver) em um buffer
  	if((tempF = fopen(TEMPF, "w+")) == NULL){
  		encerrar(TEMPF);
  	}

  	//Escrevendo a chave publica no arquivo de saida
  	geraChavePublica(chavePub, chavePriv);
  	fwrite(chavePub, TAM, 1, ArquivoSaida);
  	/*
	Como havera a concatenacao da mensagem original com o resumo gerado na funcao hash,
	sera' necessario algum parametro para delimitar o que e' a mensagem original e o que 
	e' o resumo gerado na funcao hash. Neste caso, sera' salvo no cabecalho do arquivo o 
	tamanho da mensagem original
  	*/
  	fseek(ArquivoEntrada, 0, SEEK_END);

  	//Contabilizando numero de caracteres da mensagem
  	cont = ftell(ArquivoEntrada);

  	//Transformando o valor de tamanho da mensagem em string
  	snprintf(tamMsg, 32, "%ld", cont);
  	len = strlen(tamMsg);

  	//Fazendo com que o ponteiro de leitura aponte para o inicio do arquivo novamente
  	fseek(ArquivoEntrada, 0, SEEK_SET);

  	//Criptografando o tamanho da mensagem
  	criptografiaSimetrica((byte *)tamMsg, chaveSim, len);

  	//Adicionando o \n no final da string para uma linha ser pulada apos a escrita desta
  	tamMsg[len] = '\n';
  	tamMsg[len + 1] = '\0';
  	len++;

  	//Escrevendo o tamanho da mensagem no cabecalho do arquivo de saida
  	fwrite(tamMsg, len, 1, ArquivoSaida);

  	// Escrevendo a mensagem no arquivo temporario para fazer a concatenacao com o resumo hash
  	do{
  		//Limpando o buffer do BlocoDados
  		memset(BlocoDados, 0, (TAM + 1));
    		Itens = fread(BlocoDados, 1, TAM, ArquivoEntrada);
    		if(Itens != 0) 
    		{
      			fwrite(BlocoDados, Itens, 1, tempF);
    		}
  	} while(!feof(ArquivoEntrada));


  	//Retornando o ponteiro ao inicio do arquivo de entrada
  	fseek(ArquivoEntrada, 0, SEEK_SET);

  	// === Aplicando a funcao hash + criptografia assimetrica e concatenando com a mensagem original ===
  	memset(concatHash, 0, (TAM + 1));
  	do{
  		//Limpando o buffer do BlocoDados
  		memset(BlocoDados, 0, TAM + 1);
  		Itens = fread(BlocoDados, 1, TAM, ArquivoEntrada);
    		if(Itens != 0) 
    		{
      			hashResult = hash(BlocoDados);
      			snprintf(hashString, 5, "%u", hashResult);
      			strcat(concatHash, hashString);
      			if(strlen(concatHash) == TAM){
      				//Aplicando a criptografia assimetrica com a chave privada no resumo gerado pela funcao hash
      				criptografiaAssimetrica((byte *)concatHash, chavePriv, strlen(concatHash));
      				fwrite(concatHash, strlen(concatHash), 1, tempF);
      				memset(concatHash, 0, (TAM + 1));
      			}

    		}

  	} while(!feof(ArquivoEntrada));
  	if(strlen(concatHash) > 0){
  		//Aplicando a criptografia assimetrica com a chave privada no resumo gerado pela funcao hash
      		criptografiaAssimetrica((byte *)concatHash, chavePriv, strlen(concatHash));
      		fwrite(concatHash, strlen(concatHash), 1, tempF);
  	}

  	fseek(tempF, 0, SEEK_SET);

  	// === Criptografando a mensagem + o resumo criptografado da funcao hash ===  	
  	do{
  		//Limpando o buffer do BlocoDados
  		memset(BlocoDados, 0, (TAM + 1));
    		Itens = fread(BlocoDados, 1, TAM, tempF);
    		if(Itens != 0) 
    		{
    			criptografiaSimetrica(BlocoDados, chaveSim, Itens);
      			fwrite(BlocoDados, Itens, 1, ArquivoSaida);
    		}
  	} while(!feof(tempF));

  	//Fechando e excluindo o arquivo temporario 
  	fclose(tempF);
  	if((remove(TEMPF) != 0)){
  		printf("Erro ao tentar deletar o arquivo %s\n", TEMPF);
  	}

  	fclose(ArquivoEntrada);
  	fclose(ArquivoSaida);

	return 0;
}