#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define TAM 8
#define SYMBOLS 256
#define TEMPF "arquivoTemporario2.txt"

typedef unsigned char byte;

//Esta funcao exibe uma mensagem de erro e encerra
//a execucao do programa caso haja algum problema
void encerrar(char *s){
	perror(s);
    	exit(1);
}

/*
Funcao hash para resumir uma string
Retorna um valor com 4 casas decimais sempre
para strings de ate 8 bytes de comprimento, que e' o padrao
do codigo
*/
unsigned int hash(unsigned char *str){
        unsigned int hash = 0;
        int c;

        while ((c = *str++)){
        	hash = (hash * 0.79) + c;
        }
        hash += 1000;

        return hash;
}

void decriptografiaAssimetrica(byte *bloco, byte *chave, int itens)
{
	int i;
	char temp;
	//Revertendo a ordem dos caracteres
  	for(i = 0; i < (itens / 2); i++) 
  	{
  		temp = bloco[itens - (i + 1)];
  		bloco[itens - (i + 1)] = bloco[i];
  		bloco[i] = temp;
	}
	//Finalizando a decriptografia da mensagem
	for(i = 0; i < itens; i++){
		bloco[i] = (bloco[i] + chave[i]) % SYMBOLS;
	}	
}


void decriptografiaSimetrica(byte *bloco, byte *chave, int itens)
{
	int i;
	char temp;
	//Revertendo a ordem dos caracteres
  	for(i = 0; i < (itens / 2); i++) 
  	{
  		temp = bloco[itens - (i + 1)];
  		bloco[itens - (i + 1)] = bloco[i];
  		bloco[i] = temp;
	}
	//Decriptografando a mensagem
	for(i = 0; i < itens; i++){
		if(i % 2 == 0){
			bloco[i] = bloco[i] - (chave[i] >> 1);
		}else{
			bloco[i] = bloco[i] - (chave[i] << 1);
		}
	}	
}

int main()
{
	long ltamMsg, marcador, fim;
	unsigned int hashResult;
	int i = 0;
	byte BlocoDados[TAM + 1], chavePub[TAM], chaveSim[TAM];
	char c, tamMsg[32];
  	FILE *ArquivoEntrada, *ArquivoSaida, *tempF;
  	char NomeArquivoEntrada[30], NomeArquivoSaida[30], hashString[5], compHash[5];
  	int Itens;

  	printf("Entre com o Nome do Arquivo criptografado = ");
  	scanf("%s", NomeArquivoEntrada);
  	printf("Entre com o Nome do Arquivo que ira conter a mensagem decriptografada = ");
  	scanf("%s", NomeArquivoSaida);
  	printf("Entre com a chave simetrica de 8 digitos = ");
  	scanf("%s", chaveSim);
  
 	if((ArquivoEntrada  = fopen(NomeArquivoEntrada, "rb")) == NULL){
 		encerrar(NomeArquivoEntrada);
 	}
  	if((ArquivoSaida = fopen(NomeArquivoSaida, "w+")) == NULL){
  		encerrar(NomeArquivoSaida);
  	}
  	if((tempF = fopen(TEMPF, "w+")) == NULL){
  		encerrar(TEMPF);
  	}

  	//Obtendo a chave publica
  	fread(chavePub, TAM, 1, ArquivoEntrada);

  	//Obtendo o comprimento da mensagem
  	while((c = fgetc(ArquivoEntrada)) != '\n'){
  		tamMsg[i++] = c;
  	}
  	tamMsg[i] = '\0';

  	//Decriptografando o tamanho do valor da mensagem
  	decriptografiaSimetrica((byte *)tamMsg, chaveSim, (int)strlen(tamMsg));
  	ltamMsg = atol(tamMsg);
  	
  	// === Decriptografando a mensagem + resumo hash com decriptografia simetrica
  	do{
  		//Limpando o buffer do BlocoDados
  		memset(BlocoDados, 0, (TAM + 1));
    		Itens = fread(BlocoDados, 1, TAM, ArquivoEntrada);
    		if(Itens != 0) 
    		{
    			decriptografiaSimetrica(BlocoDados, chaveSim, Itens);
      			fwrite(BlocoDados, Itens, 1, tempF);
    		}
  	} while(!feof(ArquivoEntrada));

  	//Escrevendo a mensagem decriptografada no arquivo de saída
  	fseek(tempF, 0, SEEK_SET);
  	while((ltamMsg - TAM) >= 0){
  		fread(BlocoDados, TAM, 1, tempF);
  		fwrite(BlocoDados, TAM, 1, ArquivoSaida);
  		memset(BlocoDados, 0, (TAM + 1));

  		ltamMsg -= TAM;
  	}
  	if(ltamMsg > 0){
  		fread(BlocoDados, ltamMsg, 1, tempF);
  		fwrite(BlocoDados, ltamMsg, 1, ArquivoSaida);
  		memset(BlocoDados, 0, (TAM + 1));
  	}

  	ltamMsg = marcador = ftell(tempF); //Re-obtendo o tamanho da mensagem original
  	fseek(tempF, 0, SEEK_END); //Indo para o final do arquivo pra contar quantos caracteres ele possui
  	fim = ftell(tempF); //Obtendo o numero de caracteres

  	// === Decriptografando de forma assimetrica o resumo hash ===
  	do{
  		//Limpando o buffer do BlocoDados
  		memset(BlocoDados, 0, (TAM + 1));
  		fseek(tempF, marcador, SEEK_SET);
    		Itens = fread(BlocoDados, 1, TAM, tempF);
    		if(Itens != 0) 
    		{
    			decriptografiaAssimetrica(BlocoDados, chavePub, Itens);
    			fseek(tempF, marcador, SEEK_SET);
    			//Sobrescrevendo o resumo hash criptografado no arquivo
      			fwrite(BlocoDados, Itens, 1, tempF);
      			marcador += Itens;
    		}
  	} while(marcador < fim);	

  	// === Aplicando hash na mensagem decriptografa e fazendo a comparacao
  	//com o resumo hash recebido ===
  	fseek(ArquivoSaida, 0, SEEK_SET);
  	fseek(tempF, ltamMsg, SEEK_SET);
  	do{
  		//Limpando o buffer do BlocoDados
  		memset(BlocoDados, 0, (TAM + 1));
    		Itens = fread(BlocoDados, 1, TAM, ArquivoSaida);
    		if(Itens != 0) 
    		{
    			hashResult = hash(BlocoDados);
    			snprintf(hashString, 5, "%u", hashResult);
    			memset(compHash, 0, 5);
    			fread(compHash, 4, 1, tempF);
    			//Fazendo a comparacao
    			if(!(strcmp(compHash, hashString) == 0)){
    				printf("Houve incompatibilidade dos resumos hash gerados!\n");
    			}
    		}
  	} while(!feof(ArquivoSaida));

  	//Fechando e excluindo o arquivo temporario 
  	fclose(tempF);
  	if((remove(TEMPF) != 0)){
  		printf("Erro ao tentar deletar o arquivo %s\n", TEMPF);
  	}

  	fclose(ArquivoEntrada);
  	fclose(ArquivoSaida);

	return 0;
}